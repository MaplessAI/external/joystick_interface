/*
 * MIT License
 *
 * Copyright (c) 2021 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "geometry_msgs/PointStamped.h"
#include "mapless/joystick_interface/lib_joystick_interface.hpp"
#include "ros/ros.h"
#include "sensor_msgs/Joy.h"

namespace {
const auto NODE_NAME = "command_2d_2_joystick_node";
const auto SUB_TOPIC_NAME = "command_in";
const auto PUB_TOPIC_NAME = "joystick_out";
const auto QUEUE_SIZE = 1;
}  // namespace

//------------------------------------------------------------------------------

namespace mapless {

class Command2D_2JoystickNode {
 public:
  explicit Command2D_2JoystickNode() : nh_(NODE_NAME) {
    command2_d_sub_ = nh_.subscribe(SUB_TOPIC_NAME, QUEUE_SIZE,
                                    &Command2D_2JoystickNode::subscriber, this);

    joy_pub_ = nh_.advertise<sensor_msgs::Joy>(PUB_TOPIC_NAME, QUEUE_SIZE);
  }

 private:
  void subscriber(const geometry_msgs::PointStamped::ConstPtr msg) const {
    const auto joy_msg = command_2_generic_xbox(*msg);
    joy_pub_.publish(joy_msg);
  }

  ros::Publisher joy_pub_;
  ros::Subscriber command2_d_sub_;
  ros::NodeHandle nh_;
};  // class Command2D_2JoystickNode

}  // namespace mapless

//------------------------------------------------------------------------------

int main(int argc, char *argv[]) {
  ros::init(argc, argv, NODE_NAME);

  mapless::Command2D_2JoystickNode handler;
  ROS_INFO_STREAM("Spinning " << NODE_NAME << "...");
  ros::spin();

  return EXIT_SUCCESS;
}
