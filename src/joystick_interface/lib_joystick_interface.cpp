/*
 * MIT License
 *
 * Copyright (c) 2021 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "mapless/joystick_interface/lib_joystick_interface.hpp"

#include <vector>

namespace mapless {

//------------------------------------------------------------------------------

sensor_msgs::Joy command_2_g920(
    const geometry_msgs::PointStamped& command_msg) {
  auto msg = zero_initialized_gamepad();
  msg.header = command_msg.header;
  msg.axes[static_cast<int>(G920_Axes::STEER)] = command_msg.point.y;

  auto accel = 0.0;
  auto brake = 0.0;
  if (std::signbit(command_msg.point.x)) {
    brake = (2.0 * command_msg.point.x) + 1.0;
  } else {
    accel = (-2.0 * command_msg.point.x) + 1.0;
  }

  msg.axes[static_cast<int>(G920_Axes::ACCEL)] = accel;
  msg.axes[static_cast<int>(G920_Axes::BRAKE)] = brake;

  return msg;
}

//------------------------------------------------------------------------------

geometry_msgs::PointStamped g920_2_command(const sensor_msgs::Joy& msg) {
  geometry_msgs::PointStamped command_msg;
  command_msg.header = msg.header;

  const auto accel = 0.5 * (msg.axes[static_cast<int>(G920_Axes::ACCEL)] + 1.0);
  const auto brake = 0.5 * (msg.axes[static_cast<int>(G920_Axes::BRAKE)] + 1.0);
  const auto throttle = (accel - brake);

  command_msg.point.x = throttle;
  command_msg.point.y = msg.axes[static_cast<int>(G920_Axes::STEER)];

  return command_msg;
}

//------------------------------------------------------------------------------

sensor_msgs::Joy zero_initialized_gamepad() {
  sensor_msgs::Joy msg;
  msg.header.stamp.sec = 0;
  msg.header.stamp.nsec = 0u;
  msg.header.frame_id = "";
  msg.axes = std::vector<float>(6, 0.0f);
  msg.buttons = std::vector<int>(12, 0);
  return msg;
}

//------------------------------------------------------------------------------

sensor_msgs::Joy command_2_generic_xbox(
    const geometry_msgs::PointStamped& command_msg) {
  auto msg = zero_initialized_gamepad();
  msg.header = command_msg.header;
  msg.axes[static_cast<int>(GenericXboxAxes::Y)] = command_msg.point.y;
  msg.axes[static_cast<int>(GenericXboxAxes::X)] = command_msg.point.x;
  return msg;
}

//------------------------------------------------------------------------------

geometry_msgs::PointStamped generic_xbox_2_command(
    const sensor_msgs::Joy& msg) {
  geometry_msgs::PointStamped command_msg;
  command_msg.header = msg.header;
  command_msg.point.y = msg.axes[static_cast<int>(GenericXboxAxes::Y)];
  command_msg.point.x = msg.axes[static_cast<int>(GenericXboxAxes::X)];
  return command_msg;
}

//------------------------------------------------------------------------------

sensor_msgs::Joy command_2_logitech(
    const geometry_msgs::PointStamped& command_msg) {
  auto msg = zero_initialized_gamepad();
  msg.header = command_msg.header;
  msg.axes[static_cast<int>(LogitechAxes::Y)] = command_msg.point.y;
  msg.axes[static_cast<int>(LogitechAxes::X)] = command_msg.point.x;
  return msg;
}

//------------------------------------------------------------------------------

geometry_msgs::PointStamped logitech_2_command(const sensor_msgs::Joy& msg) {
  geometry_msgs::PointStamped command_msg;
  command_msg.header = msg.header;
  command_msg.point.y = msg.axes[static_cast<int>(LogitechAxes::Y)];
  command_msg.point.x = msg.axes[static_cast<int>(LogitechAxes::X)];
  return command_msg;
}

//------------------------------------------------------------------------------

}  // namespace mapless
