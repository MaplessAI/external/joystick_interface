/*
 * MIT License
 *
 * Copyright (c) 2021 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "geometry_msgs/PointStamped.h"
#include "mapless/joystick_interface/lib_joystick_interface.hpp"
#include "ros/ros.h"
#include "sensor_msgs/Joy.h"

namespace {
const auto NODE_NAME = "joystick_2_command_2d_node";
const auto SUB_TOPIC_NAME = "joystick_in";
const auto PUB_TOPIC_NAME = "command_out";
const auto QUEUE_SIZE = 1;
}  // namespace

//------------------------------------------------------------------------------

namespace mapless {

class Joystick2Command2D_Node {
 public:
  explicit Joystick2Command2D_Node() : nh_(NODE_NAME) {
    joy_sub_ = nh_.subscribe(SUB_TOPIC_NAME, QUEUE_SIZE,
                             &Joystick2Command2D_Node::subscriber, this);

    command_2d_pub_ =
        nh_.advertise<geometry_msgs::PointStamped>(PUB_TOPIC_NAME, QUEUE_SIZE);

    if (using_g920_) {
      ROS_WARN_ONCE(
          "USING G920 INPUT: MOVE THE STEERING WHEEL AND DEPRESS ACCEL AND "
          "BRAKE PEDALS TO INITIALIZE");
    }
  }

 private:
  struct G920_Checker {
    bool steer_moved = false;
    bool accel_moved = false;
    bool brake_moved = false;
    bool update_and_check(const sensor_msgs::Joy::ConstPtr msg) {
      steer_moved = (steer_moved ||
                     (msg->axes[static_cast<int>(G920_Axes::STEER)] != 0.0));
      accel_moved = (accel_moved ||
                     (msg->axes[static_cast<int>(G920_Axes::ACCEL)] != 0.0));
      brake_moved = (brake_moved ||
                     (msg->axes[static_cast<int>(G920_Axes::BRAKE)] != 0.0));

      return steer_moved && accel_moved && brake_moved;
    }
  };

  void subscriber(const sensor_msgs::Joy::ConstPtr msg) {
    if (using_g920_ && !g920_ready_) {
      g920_ready_ = g920_checker_.update_and_check(msg);
      if (g920_ready_) {
        ROS_INFO_ONCE("G920 INITIALIZED: Node running...");
      } else {
        return;
      }
    }

    const auto command_msg = g920_2_command(*msg);
    command_2d_pub_.publish(command_msg);
  }

  bool using_g920_ = true;
  bool g920_ready_ = false;
  G920_Checker g920_checker_;

  ros::Subscriber joy_sub_;
  ros::Publisher command_2d_pub_;
  ros::NodeHandle nh_;
};  // class Joystick2Command2D_Node

}  // namespace mapless

//------------------------------------------------------------------------------

int main(int argc, char *argv[]) {
  ros::init(argc, argv, NODE_NAME);

  mapless::Joystick2Command2D_Node handler;
  ros::spin();

  return EXIT_SUCCESS;
}
