/*
 * MIT License
 *
 * Copyright (c) 2021 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef MAPLESS__JOYSTICK_INTERFACE__LIB_JOYSTICK_INTERFACE_HPP_
#define MAPLESS__JOYSTICK_INTERFACE__LIB_JOYSTICK_INTERFACE_HPP_

#include "geometry_msgs/PointStamped.h"
#include "sensor_msgs/Joy.h"

namespace mapless {

/// @brief Axis mappings for joysticks
enum class GenericXboxAxes { X = 1, Y = 3 };
enum class LogitechAxes { X = 1, Y = 0 };
enum class G920_Axes { STEER = 0, ACCEL = 1, BRAKE = 2 };

/// @brief A zero-initialized joystick message for a gamepad
/// @note Something like this:
///       https://www.logitechg.com/en-us/products/gamepads/f310-gamepad.html
sensor_msgs::Joy zero_initialized_gamepad();

/// @brief Convert to external joystick from internal control
sensor_msgs::Joy command_2_g920(const geometry_msgs::PointStamped& command_msg);

/// @brief Convert to internal control from external joystick
geometry_msgs::PointStamped g920_2_command(
    const sensor_msgs::Joy& logitech_msg);

/// @brief Convert to external joystick from internal control
sensor_msgs::Joy command_2_generic_xbox(
    const geometry_msgs::PointStamped& command_msg);

/// @brief Convert to internal control from external joystick
geometry_msgs::PointStamped generic_xbox_2_command(
    const sensor_msgs::Joy& logitech_msg);

/// @brief Convert to external joystick from internal control
sensor_msgs::Joy command_2_logitech(
    const geometry_msgs::PointStamped& command_msg);

/// @brief Convert to internal control from external joystick
geometry_msgs::PointStamped logitech_2_command(
    const sensor_msgs::Joy& logitech_msg);
}  // namespace mapless

#endif  // MAPLESS__JOYSTICK_INTERFACE__LIB_JOYSTICK_INTERFACE_HPP_
